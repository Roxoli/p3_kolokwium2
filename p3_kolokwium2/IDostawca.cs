﻿using System;
using System.Collections.Generic;
using System.Text;

namespace p3_kolokwium2
{
    interface IDostawca
    {
        Produkt wyszukaj();
        void Zamow(Produkt produkt);
    }
}
