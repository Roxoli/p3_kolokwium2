﻿using System;
using System.Collections.Generic;
using System.Text;

namespace p3_kolokwium2
{
   public class Produkt
    {
        public float cena { get; set; }
        public string nazwa { get; set; }

        public Produkt(float cena, string nazwa)
        {
            this.cena = cena;
            this.nazwa = nazwa;
        }

    }
}
