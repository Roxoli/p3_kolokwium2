﻿using System;

namespace p3_kolokwium2
{
    class Program
    {
        static void Main(string[] args)
        {
            Telefon Nokia3310 = new Telefon();
            Smartfon XPhone = new Smartfon();

            Nokia3310.Dzwon("123123123");
            XPhone.Dzwon("321321321");
            XPhone.LadujStroneWWW("www.bash.org\n");

            DostawcaJeden dostawca1 = new DostawcaJeden();
            DostawcaDwa dostawca2 = new DostawcaDwa();
            dostawca1.fillListOfProducts();
            dostawca2.fillListOfProducts();


            Console.ReadKey();
        }
    }
}
